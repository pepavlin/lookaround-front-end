
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Provider } from 'react-redux';
import configureStore from './assets/src/new_version/data/redux/configureStore';
import App from './App';
import { NavigationContainer } from '@react-navigation/native';

const store = configureStore()

export default function Index() {
    return (
        <Provider store = { store }>
            
            <NavigationContainer>
                <App/>
            </NavigationContainer>
            
        </Provider>
    
    
    );


}
