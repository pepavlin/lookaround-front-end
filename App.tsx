import React from 'react';
import HomeScreen from './assets/src/new_version/screens/HomeScreen';
import ExploreScreen from './assets/src/new_version/screens/ExploreScreen';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {Colors} from "./assets/src/new_version/design/Stylization"
import { useEffect } from 'react';
import useTargets from './assets/src/new_version/data/redux/useTargets';
import TargetData from './assets/src/new_version/data/TargetData';
import * as Location from 'expo-location';
import usePermission from './assets/src/new_version/data/redux/usePermission';
import useLocation from './assets/src/new_version/data/redux/useLocation';
import { RootState } from './assets/src/new_version/data/redux/rootState';
import Dattor from './assets/src/new_version/data/Dattor';
import Rotattor from './assets/src/new_version/Rotattor';
import * as Utils from "./assets/src/new_version/Utils"
import { AppState, Button, Platform, TouchableOpacity, Text, View, Pressable } from 'react-native';
import NearestScreen from './assets/src/new_version/screens/NearestScreen';
import { deleteLegacyDocumentDirectoryAndroid } from 'expo-file-system';
import DownloadDataScreen from './assets/src/new_version/screens/DownloadDataScreen';
import * as FeatherIcons from 'react-native-feather';

const Stack = createNativeStackNavigator();
export default function App() {

    const [targets, setTargets] = useTargets();
    const [permission, setPermission] = usePermission();
    const [location, setLocation] = useLocation();

    const navigation = useNavigation();

    useEffect(()=>{
        appInitialize();
    },[]);

    

    const appInitialize = async () =>{
        console.log("App launched.");

        const granted = await Location.requestForegroundPermissionsAsync()
        setPermission({location:granted});
        if(!granted) return;
        

        Location.watchPositionAsync({
            accuracy:100,
            distanceInterval:0
        }, (result) =>{
            const filteredLocation = {
                latitude: result.coords.latitude, 
                longitude: result.coords.longitude, 
                altitude: result.coords.altitude?result.coords.altitude:0
            }

            setLocation(filteredLocation);
            onLocationChange(filteredLocation);
        });       
    }

    const distanceInterval = 1000;
    let loadedLocation : RootState["location"] | undefined = undefined;
    const onLocationChange = async (location: RootState["location"])=>{
        location.altitude =0;
        if(loadedLocation===undefined){
            setTargets(await Dattor.get(location));
            loadedLocation = location;
            return;
        }
        if(Utils.calculateDistance(loadedLocation, location)>distanceInterval){
            setTargets(await Dattor.get(location));
            loadedLocation = location;
        }

    }


    return (
                <Stack.Navigator>
                    <Stack.Screen 
                        name="Home"
                        component={HomeScreen}
                        options={{
                            headerShown:false,
                            
                        }}
                    />
                    <Stack.Screen 
                        name="DownloadData"
                        component={DownloadDataScreen}
                        options={{
                            title: 'Stáhnout data',
                            headerTitleAlign: "left",
                            headerStyle: {
                                backgroundColor: Colors.light,
                                
                            },
                            headerTintColor: Colors.dark,
                            headerTitleStyle: {
                            fontWeight: 'bold',
                            fontSize: 20
                            },
                        }}
                    />
                    <Stack.Screen 
                        name="Nearest"
                        component={NearestScreen}
                        options={{
                            title: 'Seznam objektů',
                            headerTitleAlign: "left",
                            headerStyle: {
                                backgroundColor: Colors.light,
                                
                            },
                            headerTintColor: Colors.dark,
                            headerTitleStyle: {
                            fontWeight: 'bold',
                            fontSize: 20
                            },
                        }}
                    />
                    <Stack.Screen 
                        name="Explore"
                        component={ExploreScreen}
                        options={{
                            title: 'Okolní objekty',
                            headerTitleAlign: "left",
                            headerStyle: {
                                backgroundColor: Colors.light,
                                
                            },
                            headerTintColor: Colors.dark,
                            headerTitleStyle: {
                            fontWeight: 'bold',
                            fontSize: 20
                            },
                        }}
                        
                    />
                </Stack.Navigator>  
    );


}
