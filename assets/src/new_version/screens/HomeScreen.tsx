import React, { Component } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import {Colors, Spacing, } from "../design/Stylization"
import useTargets from "../data/redux/useTargets"
import usePermission from "../data/redux/usePermission"
import useLocation from "../data/redux/useLocation";
import { formatLocation } from "../Utils";
import Dattor from "../data/Dattor";
import NearTargetCard from "../components/NearTargetCard"

import {
    useFonts,
    SourceSansPro_200ExtraLight,
    SourceSansPro_200ExtraLight_Italic,
    SourceSansPro_300Light,
    SourceSansPro_300Light_Italic,
    SourceSansPro_400Regular,
    SourceSansPro_400Regular_Italic,
    SourceSansPro_600SemiBold,
    SourceSansPro_600SemiBold_Italic,
    SourceSansPro_700Bold,
    SourceSansPro_700Bold_Italic,
    SourceSansPro_900Black,
    SourceSansPro_900Black_Italic,
  } from '@expo-google-fonts/source-sans-pro';
import { SafeAreaView } from "react-native-safe-area-context";
import * as FeatherIcon from "react-native-feather";

interface HomeScreenProps{
    navigation: any
}
const HomeScreen = (props:HomeScreenProps) =>{

    const [targets, setTargets] = useTargets();
    const [permission] = usePermission();
    const [location] = useLocation();

    const formattedLocation = formatLocation(location);

    const onExploreButtonPress = () =>{
        if(permission.location){
            console.log("explore");
            props.navigation.navigate('Explore');
        }else{
            console.log("no permission");
        }
    }

    const onMoreButtonPress = () =>{
        props.navigation.navigate('Nearest');
    }

    const onDownloadPress = () =>{
        props.navigation.navigate("DownloadData");
    }


    let [fontsLoaded] = useFonts({
        SourceSansPro_200ExtraLight,
        SourceSansPro_200ExtraLight_Italic,
        SourceSansPro_300Light,
        SourceSansPro_300Light_Italic,
        SourceSansPro_400Regular,
        SourceSansPro_400Regular_Italic,
        SourceSansPro_600SemiBold,
        SourceSansPro_600SemiBold_Italic,
        SourceSansPro_700Bold,
        SourceSansPro_700Bold_Italic,
        SourceSansPro_900Black,
        SourceSansPro_900Black_Italic,
      });

    if(!fontsLoaded){
        return <View></View>
    }

    const v1 = 100;
    const v2 = 150;
    const v3 = 200;
    
    return (
        <SafeAreaView style={{flex:1,backgroundColor: Colors.primary}}>
            
            <View style={{flex:1,  padding: Spacing.s2, alignItems:"center"}}>

            {<Image style={{height: '120%', position:"absolute", top:0, left:0, opacity:0.7 ,resizeMode:"cover"}} source={require('.//..//..//..//images/smoke.png')}>
                
                </Image>}
            
            <TouchableOpacity style={{padding:15, position:"absolute", right:0, top:0}} onPress={onDownloadPress}>
                <FeatherIcon.Download color={Colors.dark} width={30}height={30}/>
            </TouchableOpacity>
            <View style={{flex:1, margin:Spacing.s2, justifyContent:"center"}}>
                <Text style={{textAlign:"center", fontSize:58, fontFamily:"SourceSansPro_900Black", color:Colors.white}}>LOOK</Text>
                <Text style={{textAlign:"center", fontSize:47, fontFamily:"SourceSansPro_900Black", color:Colors.white, marginTop:-20}}>AROUND</Text>
            </View>

            <View style={{flex:2, width:"100%", justifyContent:"center"}}>
                {targets&&targets.slice(0,3).map((element, index)=>{
                    return <NearTargetCard target={element} key={index}/>
                })}

                {targets&&targets.length>3&&<View style={{alignItems:"center", marginTop:5, marginBottom:10}}>
                    <TouchableOpacity style={{backgroundColor:Colors.dark, paddingHorizontal:5, borderRadius:2, opacity:0.8}} onPress={onMoreButtonPress}>
                        <Text style={{fontFamily:"SourceSansPro_400Regular", fontSize:20, color:Colors.white}}>Více</Text>
                    </TouchableOpacity>
                </View>}

                {targets&&targets.length==0&&
                    <View style={{alignItems:"center"}}>
                        <Text style={{fontFamily:"SourceSansPro_300Light", fontSize:20, color:Colors.white}}>
                            Chvilku vyčkejte. Pokud se žádné objekty neukáží, zkuste aplikaci otevřít znovu
                        </Text>
                    </View>
                }
            </View>

                
            <View style={{flex:2, justifyContent:"center", alignContent:"center"}}>
                <View style={{width:1,height:1}}>
                    

                    <View style={{backgroundColor:Colors.white, opacity:0.5, width:v2, height:v2, borderRadius:100, position:"absolute",left:0, top:0, transform:[{translateX:-v2/2},{translateY:-v2/2}]}}>
                    </View>

                    <View style={{backgroundColor:Colors.white, opacity:0.2, width:v3, height:v3, borderRadius:100, position:"absolute",left:0, top:0, transform:[{translateX:-v3/2},{translateY:-v3/2}]}}>
                    </View>

                    <TouchableOpacity onPress={onExploreButtonPress} style={{backgroundColor:Colors.secondary, opacity:1, width:v1, height:v1, borderRadius:100, borderWidth:10, borderColor:Colors.white,position:"absolute", left:0, top:0, transform:[{translateX:-v1/2},{translateY:-v1/2}]}}>
                    
                    </TouchableOpacity>

                    <TouchableOpacity onPress={onExploreButtonPress}>
                        <Image source={require('.//..//..//..//images/exploreIcon.png')}
                            style={{width:v1, height:v1, position:"absolute",left:0, top:0, transform:[{translateX:-v1/2},{translateY:-v1/2}]}}/>
                    </TouchableOpacity>
                    
                </View>
                
                
            </View>

            </View>
        </SafeAreaView>
        
    );
    
    
}

export default HomeScreen;