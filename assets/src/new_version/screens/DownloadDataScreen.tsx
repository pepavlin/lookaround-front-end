import React from "react";
import { TextInput, View , StyleSheet, TouchableOpacity, Text} from "react-native";
import { Colors, Spacing } from "../design/Stylization";
import {
    useFonts,
    SourceSansPro_200ExtraLight,
    SourceSansPro_200ExtraLight_Italic,
    SourceSansPro_300Light,
    SourceSansPro_300Light_Italic,
    SourceSansPro_400Regular,
    SourceSansPro_400Regular_Italic,
    SourceSansPro_600SemiBold,
    SourceSansPro_600SemiBold_Italic,
    SourceSansPro_700Bold,
    SourceSansPro_700Bold_Italic,
    SourceSansPro_900Black,
    SourceSansPro_900Black_Italic,
  } from '@expo-google-fonts/source-sans-pro';
import { Button } from "react-native-elements/dist/buttons/Button";
import useLocation from "../data/redux/useLocation";
import Dattor from "../data/Dattor";

const DownloadDataScreen = () => {

    const [location] = useLocation();
    const [textLatitude, onChangeTextLatidude] = React.useState(location.latitude+"");
    const [textLongitude, onChangeTextLongitude] = React.useState(location.longitude+"");
    const [textRadius, onChangeTextRadius] = React.useState("2000");
    const [buttonText, setButtonText] = React.useState("Stáhnout");

    let [fontsLoaded] = useFonts({
        SourceSansPro_200ExtraLight,
        SourceSansPro_200ExtraLight_Italic,
        SourceSansPro_300Light,
        SourceSansPro_300Light_Italic,
        SourceSansPro_400Regular,
        SourceSansPro_400Regular_Italic,
        SourceSansPro_600SemiBold,
        SourceSansPro_600SemiBold_Italic,
        SourceSansPro_700Bold,
        SourceSansPro_700Bold_Italic,
        SourceSansPro_900Black,
        SourceSansPro_900Black_Italic,
      });

    const styles = StyleSheet.create({
        textHolder:{
            width:"100%",
            height: 60,
            backgroundColor: Colors.light,
            paddingHorizontal: Spacing.s4,
            borderRadius:10,
            fontFamily: "SourceSansPro_400Regular",
            fontSize: 16
        },
        button:{
            width:"80%",
            height: 60,
            backgroundColor: Colors.primary,
            paddingHorizontal: Spacing.s4,
            borderRadius:10,
            justifyContent:"center",
            alignItems:"center"

        },
        buttonText:{
            fontFamily: "SourceSansPro_700Bold",
            color: Colors.light,
            fontSize: 20

        }
    });


    const onDownloadPress = async () =>{
        setButtonText("...");
        const downloaded = await Dattor.downloadChunkInRadius({latitude: parseFloat(textLatitude), longitude: parseFloat(textLongitude), altitude:0}, parseFloat(textRadius));
        if(downloaded){
            setButtonText("STAŽENO!");
        }else{
            setButtonText("Něco je špatně...");
        }
        console.log(downloaded);
    }

    if(!fontsLoaded){
        return <View></View>
    }
    return ( 
        <View style={{flex:1, backgroundColor:Colors.dark, justifyContent:"center"}}>
            <View style={{paddingHorizontal: Spacing.s3, alignItems:"center"}}>
            <Text style={{color:Colors.white, fontFamily:"SourceSansPro_700Bold", fontSize:30, marginBottom:25}}>
                    Stáhnout data předem
                </Text>

                <Text style={{color:Colors.white, fontFamily:"SourceSansPro_400Regular"}}>
                    Zeměpisná šířka
                </Text>
                <TextInput
                        style={[styles.textHolder, {marginBottom:15}]}
                        value={textLatitude}
                        onChangeText={onChangeTextLatidude}
                        placeholder="Zeměpisná šířka"
                
                    />
                    <Text style={{color:Colors.white, fontFamily:"SourceSansPro_400Regular"}}>
                    Zeměpisná délka
                    </Text>
                <TextInput
                    style={[styles.textHolder, {marginBottom:15}]}
                    value={textLongitude}
                    onChangeText={onChangeTextLongitude}
                    placeholder="Zeměpisná délka"
            
                />
                <Text style={{color:Colors.white, fontFamily:"SourceSansPro_400Regular"}}>
                Okruh v metrech
                </Text>
                <TextInput
                    style={[styles.textHolder]}
                    value={textRadius}
                    onChangeText={onChangeTextRadius}
                    placeholder="Okruh v metrech"
            
                />

                <TouchableOpacity style={[styles.button, {marginTop:15}]} onPress={onDownloadPress}>
                    <Text style={styles.buttonText}>
                        {buttonText}
                    </Text>
                </TouchableOpacity>
            </View>

            

        </View>
    );
}

export default DownloadDataScreen;