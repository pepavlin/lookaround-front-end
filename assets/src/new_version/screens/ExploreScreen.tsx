import React, { Component } from "react";
import { View, Text, TouchableOpacity, Animated, StatusBar, Pressable } from "react-native";
import  * as FeatherIcons from "react-native-feather";
import TargetInfoBottomDrawer, { TargetInfoBottomDrawerState } from "../components/TargetInfoBottomDrawer";
import TargetsPlayground from "../components/TargetsPlayground";
import Dattor from "../data/Dattor";
import { Colors } from "../design/Stylization";

export default class ExploreScreen extends Component{

    state ={
        playgroundRunning: true
    }

    componentDidMount(){

        const navigation = this.props.navigation;
        navigation.addListener('focus', () => {
            this.setState({playgroundRunning:true});
          });
    
        navigation.setOptions({
            headerRight: ()=>{
                return (
                    <View><Pressable onPress={() => {

                        this.setState({playgroundRunning:false});

                        
                    }}>
                        <FeatherIcons.List color={Colors.dark}/>
                    </Pressable></View>)
            }
        });
    }




    listIcon = ()=>{
        
        return 
    }

    animatedYOffset = new Animated.Value(0);
    useOffset = (offset: number) => {
        Animated.timing(this.animatedYOffset,{
            toValue: offset,
            useNativeDriver: false,
            duration:100
        }).start();
    }
    render = () => {
        StatusBar.setBarStyle('dark-content', true);
        return (
            <View style={{flex:1, backgroundColor:Colors.primary}}>
                {<TargetsPlayground animatedBottomOffset={this.animatedYOffset} running={this.state.playgroundRunning} onRunningChange={()=>{
                        this.props.navigation.push("Nearest")
                }}/>}
                <TargetInfoBottomDrawer useOffset={this.useOffset}/>
            </View>
        );
    }
    
}