import { useNavigation } from "@react-navigation/native";
import React, { Component } from "react";
import { ScrollView, StatusBar, View, Text } from "react-native";
import NearTargetCard from "../components/NearTargetCard";
import useTargets from "../data/redux/useTargets";
//import useTargets from "../data/redux/useTargets";
import { Colors, Spacing } from "../design/Stylization";

const NearestScreen = () =>{


    const [targets, setTargets] = useTargets();
    
    return (
            <View style={{backgroundColor:Colors.primary, flex:1}}>
                <ScrollView style={{flex:2, width:"100%"}} contentContainerStyle={{padding: Spacing.s4}}>
                    {targets&&targets.map((element, index)=>{
                        return <NearTargetCard target={element} key={index}/>
                    })}
                </ScrollView>
            </View>
        );
    
    
}

export default NearestScreen;