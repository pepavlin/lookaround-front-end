import { createStore } from 'redux';
import { rootReducer } from './rootState';


const configureStore = () => {
    return createStore(rootReducer);
}

export default configureStore;
