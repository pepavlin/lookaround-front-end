import { useDispatch, useSelector } from "react-redux";
import TargetData from "../TargetData";
import { actions, RootState } from "./rootState";

const useTargets = () : [TargetData[], (targets: TargetData[])=>void] =>{

    const dispatch = useDispatch();
    const lista = useSelector((state: RootState)=>{
        return state.targets;
    })

    function setTargets(targets:TargetData[]){
        dispatch ({
            type: actions.SET_TARGETS,
            payload: targets
        });

    }

    return [lista, setTargets];
}

export default useTargets;

