import { useDispatch, useSelector } from "react-redux";
import { actions, RootState } from "./rootState";

const usePermission = () : [RootState["permission"], (permission:any)=>void] =>{

    const dispatch = useDispatch();
    const data = useSelector((state: RootState)=>{
        return state.permission;
    })

    function setPermission(permission:any){

        dispatch ({
            type: actions.SET_PERMISSION,
            payload: permission
        });

        
    }

    return [data, setPermission];
}

export default usePermission;

