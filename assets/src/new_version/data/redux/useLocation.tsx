import { useDispatch, useSelector } from "react-redux";
import { roundToDecimalPlaces } from "../../Utils";
import { actions, RootState } from "./rootState";

const useLocation = (decimalPlaces?: number) : [RootState["location"], (location:RootState["location"])=>void] =>{

    const dispatch = useDispatch();
    const currentLocation = useSelector((state: RootState)=>{

        if(decimalPlaces===undefined) return state.location;

        const rounded = {
            latitude: roundToDecimalPlaces(state.location.latitude,decimalPlaces),
            longitude: roundToDecimalPlaces(state.location.longitude,decimalPlaces),
            altitude: roundToDecimalPlaces(state.location.altitude,decimalPlaces)
        }
        return rounded;
    })

    const setLocation = (location:RootState["location"]) => {

        dispatch ({
            type: actions.SET_LOCATION,
            payload: location
        });        

    }


    return [currentLocation, setLocation];
}

export default useLocation;

