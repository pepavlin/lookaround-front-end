import { useDispatch, useSelector } from "react-redux";
import TargetData from "../TargetData";
import { actions, RootState } from "./rootState";

const useFocussedTarget = () : [TargetData|undefined, (targetData: TargetData)=>void, ()=>void] =>{

    const dispatch = useDispatch();
    const targetData = useSelector((state: RootState)=>{
        return state.fucussedTarget;
    })

    function focusTarget(targetData:TargetData){
        dispatch ({
            type: actions.FOCUS_TARGET,
            payload: targetData
        });

        console.log("focus");
    }

    function unfocusTarget(){
        dispatch ({
            type: actions.FOCUS_TARGET,
            payload: undefined
        });

        console.log("unfocus");
    }

    return [targetData, focusTarget, unfocusTarget];
}

export default useFocussedTarget;

