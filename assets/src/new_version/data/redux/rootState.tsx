import TargetData from "../TargetData";

export interface RootState{
    targets: TargetData[],
    fucussedTarget?: TargetData,
    location: {latitude:number, longitude:number, altitude:number},
    permission: {location:boolean},
}
const initialState : RootState = {
    targets: [],
    location: {latitude:0, longitude:0, altitude:0},
    permission: {location:false}
};

export const rootReducer = (state = initialState, action:any) => {
    switch(action.type) {
        case actions.SET_TARGETS: 
            return {...state, targets:action.payload};
        case actions.FOCUS_TARGET: 
            return {...state, fucussedTarget:action.payload};
        case actions.SET_LOCATION: 
            return {...state, location:action.payload};
        case actions.SET_PERMISSION: 
            return {...state, permission: {...state.permission, ...action.payload}};
        default:
            return state;
    }
}

export const actions = {
    SET_TARGETS: "SET_TARGETS",
    FOCUS_TARGET: "FOCUS_TARGET",
    SET_PERMISSION: "SET_PERMISSION",
    SET_LOCATION: "SET_LOCATION"
}