import { RootState } from "./redux/rootState";

interface TargetDataJSON{
    name:string,
    latitude:number,
    longitude:number,
    altitude:number,
    description: string,
    type:string,
    images:string[],
    url: string
}
class TargetData{

    name: string = "";
    location: RootState["location"] = {
        latitude:0,
        longitude:0,
        altitude:0
    }

    description: string = "";
    type: string = "Not specified";
    imageUrls: string[] = [];

    url: string = "";

    constructor(json: TargetDataJSON){
        this.name = json.name;
        this.location = {
            latitude:json.latitude,
            longitude:json.longitude,
            altitude:0//json.altitude
        }
        this.description = json.description;
        this.type = json.type;

        this.url = json.url;

        if(Array.isArray(json.images)){
            this.imageUrls = json.images;
        }
    }


    isEqualTo(other: TargetData){
        let isSame = true;

        if(isSame) isSame = (this.name===other.name);

        if(isSame) isSame = (this.location.latitude===other.location.latitude);
        if(isSame) isSame = (this.location.longitude===other.location.longitude);
        if(isSame) isSame = (this.location.altitude===other.location.altitude);

        if(isSame) isSame = (this.description===other.description);
        if(isSame) isSame = (this.type===other.type);

        return isSame;
    }

    toJSON() : TargetDataJSON{
        return{
            name:this.name,
            latitude:this.location.latitude,
            longitude:this.location.longitude,
            altitude:this.location.altitude,
            description: this.description,
            type: this.type,
            images: this.imageUrls,
            url: this.url
        }
    }
}

export default TargetData;

