import { calculateDistance, convertDegreesToMeters } from "../Utils";
import { RootState } from "./redux/rootState";
import TargetData from "./TargetData";
import * as FileSystem from 'expo-file-system';
import { number } from "mathjs";
import fetchWithTimeout from "@gluons/react-native-fetch-with-timeout";

class Dattor{

    chunksFileUrl = "chunks.txt";
    targetsFileUrl = "targets.txt";

    serverUrlBase = "http://ironbrain.net:6119/places"
    /**
     * @param radius In meters
     */
    get = async (location: RootState["location"], radius = 3000, sort = true) : Promise<TargetData[]> => {

        await this.clearCacheIfNewVersion();

        const neededChunks = this.getChunksInRadius(location, radius);
        //console.log(neededChunks);

        const chunksAvailability = await this.getChunksAvailability(neededChunks);
        console.log(chunksAvailability);

        chunksAvailability.forEach(async (available, index) => {
            if(!available) await this.downloadChunk(neededChunks[index]);
        });


        let arr = await this.loadChunksFromCache(neededChunks);

        if(sort){
            const sorted = arr.sort((a,b)=>{
                const distA = calculateDistance(location, a.location);
                const distB = calculateDistance(location, b.location);
                return distA<distB?-1:1;
            });
            arr = sorted;
        }

        return arr.filter((targetData: TargetData)=>{
            return calculateDistance(location, targetData.location)<radius;
        })

    }

    private async loadChunksFromCache(chunks: Chunk[]) : Promise<TargetData[]>{
        const url = FileSystem.documentDirectory+this.targetsFileUrl;
        const info = await FileSystem.getInfoAsync(url);

        let allTargets : any[] = [];
        if(info.exists){
            const content = JSON.parse(await FileSystem.readAsStringAsync(url));
            
            allTargets = "targets" in content ? content.targets : [];
        }

        const wantedTargets : TargetData[] = [];
        allTargets.forEach((target)=>{
            const chunk = new Chunk(target.chunk);
            
            let isWanted = false;
            chunks.forEach((wantedChunk)=>{
                if(!isWanted){
                    if(chunk.isEqualTo(wantedChunk))isWanted=true;
                }
            })

            if(true||isWanted){
                const targetData = new TargetData(target.data);
                wantedTargets.push(targetData);
            }
        })

        
        /*console.log("All targets.",allTargets);
        console.log("Wanted.",wantedTargets);*/


        return wantedTargets;
    }

    private async getServerDataVersion() : Promise<string|undefined>{
        const url = this.serverUrlBase+`/version`;
        try {
            const response = await fetchWithTimeout(url,{},{timeout:1000});
            return (await response.json()).version;
        } catch (error) {
            return undefined;
        }
    }
    private async clearCacheIfNewVersion(){
        const serverDataVersion = await this.getServerDataVersion();
        let version : string|undefined = undefined;

        if(serverDataVersion===undefined)return;

        const url = FileSystem.documentDirectory+this.chunksFileUrl;
        const info = await FileSystem.getInfoAsync(url);
        if(info.exists){
            const content = JSON.parse(await FileSystem.readAsStringAsync(url));
            
            version = "version" in content ? content.version : undefined;
        }

        console.log("Version is same:",version==serverDataVersion);
        if(version!=serverDataVersion){
            console.log("Cleared");
            this.clearCache();
        }

    }

    private async downloadChunk(chunk: Chunk, timeout:number = 4000) : Promise<boolean>{
        const url = this.serverUrlBase+`/chunk?x=${chunk.x}&y=${chunk.y}&size=${Chunk.size}`;
        console.log(url);

        let dataArr : any[] = [];
        try {
            const response = await fetchWithTimeout(url,{},{timeout:timeout});
            dataArr = await response.json();
        } catch (error) {
            /*console.log("Something went wrong during a fetching.");
            console.log(error);*/
            return false;
        }

        const targetDataArr : TargetData[] = [];
        dataArr.forEach((json)=>{
            targetDataArr.push(new TargetData(json));
        })

        await this.addTargetDataArrayToCache(targetDataArr);

        this.addChunkToCache(chunk);

        return true;

    }

    private async addChunkToCache(chunk: Chunk){
        const url = FileSystem.documentDirectory+this.chunksFileUrl;

        const info = await FileSystem.getInfoAsync(url);

        let availableChunks : any[] = [];
        if(info.exists){
            const content = JSON.parse(await FileSystem.readAsStringAsync(url));
            
            availableChunks = "chunks" in content ? content.chunks : [];
        }

        const serverDataVersion = await this.getServerDataVersion();
        availableChunks.push(chunk);
        const result = JSON.stringify({chunks: availableChunks, version: serverDataVersion});
        await FileSystem.writeAsStringAsync(url, result);


    }
    

    private async addTargetDataArrayToCache(targetDataArr: TargetData[]){
        const url = FileSystem.documentDirectory+this.targetsFileUrl;
        const info = await FileSystem.getInfoAsync(url);

        let currentTargets : any[] = [];
        if(info.exists){
            const content = JSON.parse(await FileSystem.readAsStringAsync(url));
            currentTargets = "targets" in content ? content.targets : [];
        }

        const allTargetData : TargetData[] = [];
        currentTargets.forEach((json)=>{
            allTargetData.push(new TargetData(json.data));
        })
        targetDataArr.forEach((targetData)=>{
            let alreadyExists = false;
            allTargetData.forEach((other)=>{
                if(!alreadyExists) alreadyExists=targetData.isEqualTo(other);
            })
            if(!alreadyExists) allTargetData.push(targetData);
        })



        const result : any[] = [];
        allTargetData.forEach((targetData)=>{
            const chunk = Chunk.createByLocation({
                latitude: targetData.location.latitude,
                longitude: targetData.location.longitude, 
                altitude: targetData.location.altitude});


            result.push({
                chunk: [chunk.x, chunk.y],
                data: targetData.toJSON()
            })
        })

        const stringified = JSON.stringify({targets: result});
        

        await FileSystem.writeAsStringAsync(url, stringified);
    }

    private getChunksInRadius(location: RootState["location"], radius: number) : Chunk[]{

        const chunks : Chunk[] = []
        const baseDist = Math.ceil(radius / Chunk.size);
        const baseChunk = Chunk.createByLocation(location);
        chunks.push(baseChunk);

        for(let x=0; x<baseDist; x++){
            for(let y=0; y<baseDist; y++){
                if(x==0&&y==0)continue;

                const dist = Math.ceil(Math.sqrt(x*x+y*y));
                if(true||dist<=baseDist){
                    chunks.push(new Chunk([baseChunk.x+x, baseChunk.y+y]))
                    chunks.push(new Chunk([baseChunk.x-x, baseChunk.y-y]))
                    chunks.push(new Chunk([baseChunk.x-x, baseChunk.y+y]))
                    chunks.push(new Chunk([baseChunk.x+x, baseChunk.y-y]))
                }
            }
        }
        return chunks;
    }

    private async getChunksAvailability(chunks: Chunk[]) : Promise<boolean[]>{

        const url = FileSystem.documentDirectory+this.chunksFileUrl;

        const info = await FileSystem.getInfoAsync(url);

        let availableChunks : any[] = [];
        if(info.exists){
            const content = JSON.parse(await FileSystem.readAsStringAsync(url));
            
            availableChunks = "chunks" in content ? content.chunks : [];
        }

        
        const chunksAvailability : boolean[] = [];
        chunks.forEach((chunk)=>{
            let foundInAvailableOnes = false;
            availableChunks.forEach((availableOne)=>{
                if(chunk.isEqualTo(availableOne))foundInAvailableOnes=true;
            })
            chunksAvailability.push(foundInAvailableOnes);
        })

        return chunksAvailability;
    }

    clearCache = async () =>{
        const url1 = FileSystem.documentDirectory+this.chunksFileUrl;
        const info1 = await FileSystem.getInfoAsync(url1);
        if(info1.exists) await FileSystem.deleteAsync(url1);

        const url2 = FileSystem.documentDirectory+this.targetsFileUrl;
        const info2 = await FileSystem.getInfoAsync(url2);
        if(info2.exists) await FileSystem.deleteAsync(url2);

        console.log(`Cache cleared. First file exists as ${await (await FileSystem.getInfoAsync(url1)).exists}, second as ${await (await FileSystem.getInfoAsync(url2)).exists}`);

        
    }

    downloadChunkInRadius = async (location: RootState["location"], radius: number) =>{
        let ok = true;
        const chunks = this.getChunksInRadius(location, radius);
        for(let i=0; i<chunks.length; i++){

            if(!(await this.downloadChunk(chunks[i])))ok=false;
        }

        return ok;
    }

}

class Chunk{ 
    static size:number = 2000;

    x: number = 0;
    y: number = 0;
    constructor(coords: number[]){
        this.x = coords[0];
        this.y = coords[1];
    }

    static createByLocation(location: RootState["location"]) : Chunk{
        const mtrs = convertDegreesToMeters({lat: location.latitude, lng: location.longitude});
        return new Chunk([Math.round(mtrs[1]/this.size),Math.round(mtrs[0]/this.size)]);
    }

    isEqualTo(chunk: Chunk){
        return this.x==chunk.x&&this.y==chunk.y;
    }
}

export default new Dattor();