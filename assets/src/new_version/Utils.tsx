import { RootState } from "./data/redux/rootState";
import Vector3 from "./Vector3";
import * as Location from 'expo-location';


export const roundToDecimalPlaces = (num:number, count: number = 2) => {
    return Math.round(num*Math.pow(10, count))/Math.pow(10, count);
}

export const formatLocation = (location: {latitude:number, longitude:number, altitude?:number}) : {latitude:string, longitude:string, altitude?:string}=>{
    const lat = Math.abs(location.latitude);
    const latD = Math.floor(lat);
    const latM = Math.floor((lat-latD)*60);
    const latS = roundToDecimalPlaces(((lat-latD)*60-latM)*60,0);
    const latChar = (location.latitude>0?"S":"J");

    const lng = Math.abs(location.longitude);
    const lngD = Math.floor(lng);
    const lngM = Math.floor((lng-lngD)*60);
    const lngS = roundToDecimalPlaces(((lng-lngD)*60-lngM)*60,0);
    const lngChar = (location.longitude>0?"V":"Z");


    return{
        latitude: `${latD}°${latM}'${latS}" ${latChar}`,
        longitude: `${lngD}°${lngM}'${lngS}" ${lngChar}`,
        altitude: location.altitude===undefined?location.altitude:(Math.round(location.altitude)+" m")
    }
}

export const formatDistance = (distanceInMeters: number) => {

    if(distanceInMeters<1000)return Math.round(distanceInMeters)+" m";
    else{
        return Math.round(distanceInMeters/10)/100+" km";
    }
}

export const calculateDistance = (a: RootState["location"],b: RootState["location"]) =>{
    const lat1 = a.latitude;
    const lat2 = b.latitude;
    const lon1 = a.longitude;
    const lon2 = b.longitude;

    const R = 6371e3; // metres
    const φ1 = lat1 * Math.PI/180; // φ, λ in radians
    const φ2 = lat2 * Math.PI/180;
    const Δφ = (lat2-lat1) * Math.PI/180;
    const Δλ = (lon2-lon1) * Math.PI/180;
    
    const e = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
              Math.cos(φ1) * Math.cos(φ2) *
              Math.sin(Δλ/2) * Math.sin(Δλ/2);
    const c = 2 * Math.atan2(Math.sqrt(e), Math.sqrt(1-e));
    
    const d = R * c; // in metres
    return d;
}

export const convertDegreesToMeters = (degrees: {lat:number, lng: number}) : number[] => {
    const result = [0,0];
    result[0] = degrees.lat*111111;
    result[1] = degrees.lng*(111111*Math.cos(degrees.lat));

    return result;
}

export const getLocationVector = (v1:{latitude:number,longitude:number, altitude: number},v2:{latitude:number,longitude:number, altitude:number}) : Vector3 => {
    if(!("altitude"in v1))v1.altitude=0;
    if(!("altitude"in v2))v2.altitude=0;
    
    const middlePoint = {latitude:v1.latitude,longitude: v2.longitude, altitude: (v1.altitude+v2.altitude)/2};
    return new Vector3(calculateDistance(v1, middlePoint)*Math.sign(v1.longitude-v2.longitude), v2.altitude-v1.altitude,calculateDistance(v2,middlePoint)*Math.sign(v1.latitude-v2.latitude));
}

export const GetNorthDirection = async () : Promise<number> =>{
    const headingInfo = await Location.getHeadingAsync();
    return headingInfo.magHeading*Math.PI/180;
}