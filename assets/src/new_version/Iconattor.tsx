import TargetData from "./data/TargetData";
import { View , Text} from "react-native"

const targetIcons : string[][] = [
    ["Kříž", "✞"],
    ["Kaple","⛪"],
    ["Pomník padlým", "📍"],
    ["Pomník", "📍"],
    ["Křížový kámen", "🪨"],
    ["Pamětní deska", "🧾"]
]

const getIconEmojiByTargetType = (type: string) : string => {

    let emoji = "🏛";

    targetIcons.forEach(element => {
        if(element[0]==type) emoji = element[1];
    });

    return emoji;

}

export const getIconByTarget = (target: TargetData, size: number) =>{
    return <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
        <Text style={{fontSize:size}}>
        {getIconEmojiByTargetType(target.type)}
        </Text>
    </View>
}