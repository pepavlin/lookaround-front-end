import * as math from "mathjs";
export const FromYawPitchRoll = (yaw:number, pitch:number,roll:number) : math.Matrix => {

    const matrix = math.matrix([
        [Math.cos(yaw)*Math.cos(pitch), Math.cos(yaw)*Math.sin(pitch)*Math.sin(roll)-Math.sin(yaw)*Math.cos(roll),Math.cos(yaw)*Math.sin(pitch)*Math.cos(roll)+Math.sin(yaw)*Math.sin(roll)],
        [Math.sin(yaw)*Math.cos(pitch), Math.sin(yaw)*Math.sin(pitch)*Math.sin(roll)+Math.cos(yaw)*Math.cos(roll),Math.sin(yaw)*Math.sin(pitch)*Math.cos(roll)-Math.cos(yaw)*Math.sin(roll)],
        [-Math.sin(pitch), Math.cos(pitch)*Math.sin(roll), Math.cos(pitch)*Math.cos(roll)]
    ]);
    return matrix;
}

export const FromQuaternion = (qx : number, qy : number, qz : number, qw : number) : math.Matrix => {
    
    const matrix = math.matrix([
        [1 - 2*qy*qy - 2*qz*qz,	2*qx*qy - 2*qz*qw,	2*qx*qz + 2*qy*qw],
        [2*qx*qy + 2*qz*qw,	1 - 2*qx*qx - 2*qz*qz,	2*qy*qz - 2*qx*qw],
        [2*qx*qz - 2*qy*qw,	2*qy*qz + 2*qx*qw,	1 - 2*qx*qx - 2*qy*qy]
    ]);
    return matrix;
}