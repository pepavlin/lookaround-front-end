

import * as math from "mathjs";
import { Platform } from "react-native";
import { RootState } from "./data/redux/rootState";
import TargetData from "./data/TargetData";
import * as Matrix from "./Matrix";
import * as Utils from "./Utils";
import Vector3 from "./Vector3";

class Rotattor{
    startNorthDirectionInRadians = 0;

    public getMatrixFromMotionResult(result : any) : math.Matrix{

        const newRotation = {...result.rotation};
        newRotation.alpha += this.startNorthDirectionInRadians;


        const matrixAlpha = Matrix.FromYawPitchRoll(0,-newRotation.alpha,0);
        const matrixBeta = Matrix.FromYawPitchRoll(0,0,-newRotation.beta);
        const matrixGamma  = Matrix.FromYawPitchRoll(-newRotation.gamma,0,0);


        const matrixSimple = math.multiply(matrixAlpha,math.multiply(matrixBeta,matrixGamma));
        const correctionMatrix = Matrix.FromYawPitchRoll(Math.PI/2*0,0,Math.PI/2);
        const matrix = math.multiply(matrixSimple, correctionMatrix);

        return matrix;

    }

    public calculateTargetRelativePositionAndRotation(target : TargetData, location: RootState["location"], matrix: math.Matrix, displayRatio: number, projectionDistance: number = 1) : [{left:number, top:number}, number, boolean]{

        
        const direction = Utils.getLocationVector(location, target.location);
        const relativeDirection = direction.RotateByMatrix(math.transpose(matrix)).Normalize();

        /*const isInFront = relativeDirection.z<0;
        this.setState({visible:isInFront});
        if(!isInFront) return;
*/
        const coef = projectionDistance/relativeDirection.z;
        const pointInDistance = relativeDirection.MultiplyBy(coef);


        const pointRatio = Math.abs(pointInDistance.x/pointInDistance.y);

        const relativeWidth = displayRatio;
        const relativeHeight = 1

        
        let pointOnDisplay = pointInDistance;

        if(Math.abs(pointInDistance.x)>relativeWidth||Math.abs(pointInDistance.y)>relativeHeight){
            pointOnDisplay = pointInDistance.MultiplyBy(Math.abs(relativeWidth/pointInDistance.x));
            if(Math.abs(pointOnDisplay.y)>relativeHeight) 
                pointOnDisplay = pointInDistance.MultiplyBy(Math.abs(relativeHeight/pointInDistance.y));

        }
        
        if(relativeDirection.z>0)pointOnDisplay = pointOnDisplay.MultiplyBy(-1);
        

        pointOnDisplay.x/=relativeWidth;
        pointOnDisplay.y/=relativeHeight;

        const p = pointOnDisplay.DivideBy(2);
        const relativePosition = {left:p.x+0.5,top:(0.5-p.y)};

        const rotation = Math.atan2(p.x,p.y);

        return [relativePosition,rotation, relativeDirection.z<0];
    }
}

export default new Rotattor();