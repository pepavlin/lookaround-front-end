import * as math from "mathjs";

export default class Vector3{
    public static forward = new Vector3(0,0,1);
    public static backward = new Vector3(0,0,-1);
    public static left = new Vector3(-1,0,0);
    public static right = new Vector3(1,0,0);
    public static down = new Vector3(0,-1,0);
    public static up = new Vector3(0,1,0);


    public x:number;
    public y:number;
    public z:number;

    constructor(x:number = 0,y:number = 0,z:number = 0){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public Normalize() : Vector3{
        const m = this.Magnitude();
        return Vector3.Divide(this,m);
    }

    public Magnitude() : number{
        return Math.pow(this.x*this.x+this.y*this.y+this.z*this.z,1/2);
    }

    
    public MultiplyBy(n:number) : Vector3{
        return Vector3.Multiply(this,n);
    }
    public static Multiply(v:Vector3,n:number){
        return new Vector3(v.x*n,v.y*n,v.z*n)
    }


    public static Divide(v:Vector3,n:number){
        return new Vector3(v.x/n,v.y/n,v.z/n)
    }

    public DivideBy(n:number) : Vector3 {return Vector3.Divide(this,n)}

    public RotateByMatrix(m:math.Matrix) : Vector3{
        const v = this.ToMatrix();
        const m0 = math.multiply(m,v);
        return Vector3.FromMatrix(m0);
    }
    
    public ToMatrix() : math.Matrix{
        return math.matrix([[this.x],[this.y],[this.z]]);
    }

    public static FromMatrix(m:math.Matrix):Vector3{
        return new Vector3(m.get([0,0]),m.get([1,0]),m.get([2,0]))
    }

    
    public Add(v:Vector3){
        return Vector3.Add(this,v);
    }
    public static Add(v1:Vector3,v2:Vector3) : Vector3{
        return new Vector3(v1.x+v2.x,v1.y+v2.y,v1.z+v2.z);
    }
}