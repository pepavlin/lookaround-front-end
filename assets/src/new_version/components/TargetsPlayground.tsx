import { DeviceMotion } from "expo-sensors";
import math from "mathjs";
import React, { createRef, useLayoutEffect, useRef } from "react";
import { View , Text, ScrollView, Animated, Platform} from "react-native";
import TargetComponent from "../components/TargetComponent";
import useTargets from "../data/redux/useTargets";
import TargetData from "../data/TargetData";
import {Colors} from "../design/Stylization"
import * as Matrix from "../Matrix";
import Vector3 from "../Vector3";
import Rotattor from "../Rotattor";
import useLocation from "../data/redux/useLocation";
import { useEffect } from "react";
import * as Utils from "../Utils"

const TargetsPlayground = (props:{animatedBottomOffset: Animated.Value, running:boolean, onRunningChange:()=>void}) => {

    const [targets] = useTargets();
    const [location] = useLocation();

    let [width, height] = [10,10];

    

    const animatedXY : Animated.ValueXY[] = useRef([]).current;
    const animatedRotation : Animated.Value[] = useRef([]).current;
    const animatedVisibility : Animated.Value[] = useRef([]).current;


    

    let startNorthDirectionInDegrees = 0;
    useEffect(()=>{

        (async function(){
            if(Platform.OS === 'ios'){
                startNorthDirectionInDegrees = ((await Utils.GetNorthDirection())*180)/Math.PI;
            }
        })();
        
        
        DeviceMotion.setUpdateInterval(100);

        return ()=>{
        }
    },[])

    useEffect(()=>{
        if(props.running){
            DeviceMotion.addListener(rotationListener);
            return;
        }

        targets.forEach((target, index)=>{
            animatedXY[index].stopAnimation();
            animatedRotation[index].stopAnimation();
            animatedVisibility[index].stopAnimation();
        })
        DeviceMotion.removeAllListeners();

        props.onRunningChange();
    },[props.running])


    const rotationListener = async (result:any) => {

        if(result.rotation?.alpha===undefined)return;

        result.rotation.alpha += startNorthDirectionInDegrees;


        updateViewSize();

        const matrix = Rotattor.getMatrixFromMotionResult(result);
        const displayRatio = (width)/(height);
        targets.forEach((target, index)=>{
            //console.log("ratio",displayRatio, width, height);
            
            const [relativePos, rotation, front] = Rotattor.calculateTargetRelativePositionAndRotation(target, location, matrix, displayRatio);
            
            Animated.spring( animatedXY[index],{
                    toValue:{x:relativePos.left*width, y:relativePos.top*height},
                    tension:100,
                    useNativeDriver:false
            }) .start();

            Animated.spring(animatedRotation[index],{
                toValue: rotation,
                useNativeDriver: false
            }).start();

            const visible = front||!(Math.abs(relativePos.left-0.5)<0.49&&Math.abs(relativePos.top-0.5)<0.49);
            Animated.timing(animatedVisibility[index],{
                toValue: visible?1:0,
                useNativeDriver: false,
                duration:10
            }).start();
        }) 
    }


    const focusTarget = (data: TargetData) =>{

    }

    const animViewRef = useRef();
    const updateViewSize = () =>{

        if(animViewRef.current){
            animViewRef.current!.measure((a, b, _width, _height, px, py)=>{
                 width = _width;
                 height = _height;
            });
        }

    }

    
    
    return(
        <Animated.View style={{flex:1, padding: 30}} collapsable={false} >

            {<View style={{flex:1}}>
                <Animated.View  style={{flex:1,marginBottom: props.animatedBottomOffset}} ref={animViewRef} collapsable={false} >
                    {
                    targets.map((targetData, index)=>{

                        if(animatedXY[index]===undefined)
                            animatedXY[index] = (new Animated.ValueXY({x:0,y:0}));

                        if(animatedRotation[index]===undefined)
                        animatedRotation[index] = (new Animated.Value(0));

                        if(animatedVisibility[index]===undefined)
                        animatedVisibility[index] = (new Animated.Value(1));
                        

                        return <TargetComponent 
                                    key={index} 
                                    data={targetData}
                                    onPress={()=>{focusTarget(targetData)}}
                                    animatedXY={animatedXY[index]}
                                    animatedRotation={animatedRotation[index]}
                                    animatedVisibility={animatedVisibility[index]}/>
                    })
                }
                </Animated.View>
            
            </View>}
            
        </Animated.View>
    );
}

export default TargetsPlayground;