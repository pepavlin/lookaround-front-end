import React from "react";
import { View , Text, TouchableOpacity, Animated, TouchableHighlight} from "react-native";
import * as Utils from "../Utils";
import * as Iconattor from "../Iconattor";
import useFocussedTarget from "../data/redux/useFocussedTarget";
import useLocation from "../data/redux/useLocation";
import TargetData from "../data/TargetData";
import { Colors, FontWeight, Spacing, Width } from "../design/Stylization";
import { calculateDistance } from "../Utils";

interface TargetComponentProps{
    data: TargetData,
    onPress?: ()=>void,
    animatedXY?: Animated.ValueXY,
    animatedRotation?: Animated.Value,
    animatedVisibility? : Animated.Value
}

function TargetComponent(props:TargetComponentProps){

    const [focussedTarget, focusTarget, onfocusTarget] = useFocussedTarget();
    const [location] = useLocation();

    const onPress = ()=>{
        
        if(isFocussed())
            onfocusTarget();
        else 
            focusTarget(props.data);

            
        if(props.onPress) props.onPress()
    }

    const isFocussed = () =>{
        return focussedTarget!==undefined&&focussedTarget.isEqualTo(props.data);
    }

    if(props.animatedRotation===undefined)props.animatedRotation = new Animated.Value(0);

    const size = 50;

    return(
        <Animated.View style={[
            (props.animatedXY&&props.animatedXY.getLayout()),
            {position:"absolute", width:100, height:100}, {alignItems:"center", justifyContent:"center", opacity: props.animatedVisibility},
            {transform:[{translateX:-50}, {translateY:-50}]}
            
        ]} collapsable={false}>
            <View style={{width:1, height:1}}>
            {<Animated.View  collapsable={false} style={[
                    {transform: [
                        {translateX:-size/2}, {translateY:-size/2},
                        {rotate: props.animatedRotation?props.animatedRotation.interpolate({
                        inputRange: [0,Math.PI*2],
                        outputRange: ["0deg", "360deg"]
                    }):"0"}
                ]},
                {width: size, height:size, position:"absolute", left:0, top:0}
                ]}>
                    <View style={{flex:1, margin:9,backgroundColor:Colors.secondary, transform:[{translateY:-10},{rotate:Math.PI/4+"rad"},]}}>

                    </View>

            </Animated.View>}

                <TouchableHighlight onPress={onPress} underlayColor={Colors.dark}
                    style={[
                        {width: size, height:size, backgroundColor: Colors.light, borderRadius:100,position:"absolute", left:0, top:0, borderWidth:isFocussed()?5:0, borderColor:Colors.secondary},
                        {transform:[{translateX:-size/2}, {translateY:-size/2}]},
                        {shadowOffset:{width:0, height:0},shadowColor:"black", shadowRadius:4, shadowOpacity:0.5}]}>

                    <View style={{flex:1}}>
                        {Iconattor.getIconByTarget(props.data, size*0.7)}                    
                    </View>
                    </TouchableHighlight>
            </View>
             
        </Animated.View>
        
    )
}

export default TargetComponent;