import React, { useEffect, useState } from "react";
import { Animated, View, Text, TouchableOpacity, ScrollView, Image, StyleSheet, Linking} from "react-native";
import * as FeatherIcons from "react-native-feather";
import useFocussedTarget from "../data/redux/useFocussedTarget";
import { Colors, FontSize, Spacing, Width } from "../design/Stylization";
import {
    useFonts,
    SourceSansPro_200ExtraLight,
    SourceSansPro_200ExtraLight_Italic,
    SourceSansPro_300Light,
    SourceSansPro_300Light_Italic,
    SourceSansPro_400Regular,
    SourceSansPro_400Regular_Italic,
    SourceSansPro_600SemiBold,
    SourceSansPro_600SemiBold_Italic,
    SourceSansPro_700Bold,
    SourceSansPro_700Bold_Italic,
    SourceSansPro_900Black,
    SourceSansPro_900Black_Italic,
  } from '@expo-google-fonts/source-sans-pro';
import * as Utils from "../Utils";
import useLocation from "../data/redux/useLocation";
import { Prompt_600SemiBold } from "@expo-google-fonts/dev";

export enum TargetInfoBottomDrawerState {
    Closed = 0,
    Head = 1,
    Whole = 2,
  }

interface TargetInfoBottomDrawerProps {
    useOffset: (offset: number)=>void;
}
const TargetInfoBottomDrawer: React.FunctionComponent<TargetInfoBottomDrawerProps> = (props) =>{

    let [height, setHeight] = useState(400);
    const headHeight = 110;

    let [focussedTarget] = useFocussedTarget();
    let [location] = useLocation();

    
    const yOffset = React.useRef(new Animated.Value(0)).current;
    const currentStateRef = React.useRef(TargetInfoBottomDrawerState.Closed);

    const setDrawerState = (value: TargetInfoBottomDrawerState)=>{
        currentStateRef.current = value;

        const toValue : number = value>1?value/2*height:value*headHeight;
        Animated.spring(yOffset, {
          toValue: -toValue,
          tension:20,
          useNativeDriver: true,
        }).start();

        props.useOffset(toValue);
    }

    useEffect(()=>{
        setDrawerState(currentStateRef.current);
    },[height])

    const onLayout = (event:any) =>{
        const h = event.nativeEvent.layout.height;
        const c = h+headHeight;
        if(height!=c){
            setHeight(c);
            console.log("ss");
        }

        
    }

    useEffect(()=>{
        if(focussedTarget==null)
            setDrawerState(TargetInfoBottomDrawerState.Closed);
        else{
            if(currentStateRef.current==TargetInfoBottomDrawerState.Whole){
                setDrawerState(TargetInfoBottomDrawerState.Whole);
            }else{
                setDrawerState(TargetInfoBottomDrawerState.Head);
            }
        }
        
    },[focussedTarget]);

    useEffect(()=>{
        if(currentStateRef.current==TargetInfoBottomDrawerState.Whole){
            setDrawerState(TargetInfoBottomDrawerState.Head);
        }else{
            setDrawerState(currentStateRef.current);

        }
    },[]);

    const onExpandPress = () =>{
        if(currentStateRef.current==TargetInfoBottomDrawerState.Head){
            setDrawerState(TargetInfoBottomDrawerState.Whole);
        }else{
            setDrawerState(TargetInfoBottomDrawerState.Head);
        }
    }

    const imageComponent = (imageUrl: string|null) =>{
        const imageComponentStyles = StyleSheet.create({
            main:{
                height:180, 
                aspectRatio:1, 
                borderRadius:10, 
                marginRight:10, 

                borderColor:Colors.dark, 
                backgroundColor:Colors.lightGrey,
    
                opacity:1,
    
                shadowColor:Colors.dark, 
                shadowRadius:2,  
                shadowOpacity:0.3,
                shadowOffset:{width: 2, height: 4}
            }
        })
        return(
            <View key={imageUrl}
                style={[{borderWidth:0}, imageComponentStyles.main]}>

                {imageUrl&&<Image source={{uri: imageUrl}} style={[imageComponentStyles.main,{marginLeft:0}]} />}
                {!imageUrl&&
                <View style={[{flex:1,opacity:1, alignItems:"center", justifyContent: "center"}]}>
                    <Text>No more images.</Text>
                </View>
                }
            </View>
        )
    }


    let [fontsLoaded] = useFonts({
        SourceSansPro_200ExtraLight,
        SourceSansPro_200ExtraLight_Italic,
        SourceSansPro_300Light,
        SourceSansPro_300Light_Italic,
        SourceSansPro_400Regular,
        SourceSansPro_400Regular_Italic,
        SourceSansPro_600SemiBold,
        SourceSansPro_600SemiBold_Italic,
        SourceSansPro_700Bold,
        SourceSansPro_700Bold_Italic,
        SourceSansPro_900Black,
        SourceSansPro_900Black_Italic,
      });

    const openUrl = (url:string) =>{
        Linking.openURL(url);        
    }

    if(!fontsLoaded){
        return <View></View>
    }

    return(
        <Animated.View 
            style={[{
                width: '100%',
                height: height+100,
                backgroundColor: Colors.light,
                borderRadius: 25,
                borderBottomLeftRadius:0,
                borderBottomRightRadius:0,
                position: 'absolute',
                bottom: -height-100,
        
                transform: [{ translateY: yOffset }],
                }]}>

            <View style={{height: headHeight, paddingHorizontal: Spacing.s3, paddingBottom:0, flexDirection:"row"}}>
                {focussedTarget&&<View style={{flex:1,justifyContent:"center"}}>
                    <Text style={{fontSize: 20, fontFamily: "SourceSansPro_700Bold"}} numberOfLines={2}>{focussedTarget.name}</Text>
                    <Text style={{fontSize: 15, fontFamily: "SourceSansPro_600SemiBold"}} numberOfLines={1}>{Utils.formatDistance(Utils.calculateDistance(location,focussedTarget.location))}</Text>
                    
                </View>}
                <TouchableOpacity onPress={onExpandPress}
                    style={{flex:0, width: Width.w4, height: Width.w4, marginTop:Spacing.s3,borderRadius:Width.w4, backgroundColor:Colors.lightGrey, alignItems:"center", justifyContent:"center"}}>
                    <FeatherIcons.ChevronUp color={Colors.dark} width={Width.w4*0.8} height={Width.w4*0.8}/>
                </TouchableOpacity>
            </View>
            {focussedTarget&&<View style={{flex:0}} onLayout={onLayout}>

                {focussedTarget.description!=""&&
                <Text style={{paddingHorizontal: Spacing.s3, marginBottom:Spacing.s3, fontFamily: "SourceSansPro_400Regular"}}>{focussedTarget.description}</Text>}


                    {focussedTarget.url!=""&&
                    <TouchableOpacity style={{marginHorizontal: Spacing.s3, marginBottom:Spacing.s4, backgroundColor:Colors.lightGrey, 
                    borderRadius:15, justifyContent:"center", alignContent:"center",
                    padding:Spacing.s5, paddingHorizontal:Spacing.s3,
                    flexDirection:"row"}} onPress={()=>{if(focussedTarget)openUrl(focussedTarget.url)}}>
                    
                    
                    <Text style={{fontFamily: "SourceSansPro_400Regular", alignSelf:"center"}} numberOfLines={1}>{focussedTarget.url}</Text>
                    <FeatherIcons.Link2 color={Colors.dark} style={{marginLeft:10}}></FeatherIcons.Link2>
                </TouchableOpacity>}
                


                <ScrollView horizontal={true} contentContainerStyle={{paddingLeft:Spacing.s3, marginBottom:Spacing.s3}} showsHorizontalScrollIndicator={false} snapToStart={true}>
                    {focussedTarget.imageUrls.map((imageUrl)=>{
                        return imageComponent(imageUrl);
                    })}
                    
                    {focussedTarget.imageUrls.length>0&&imageComponent(null)}
                </ScrollView>

            </View>}


        </Animated.View>
    );
}

export default TargetInfoBottomDrawer;