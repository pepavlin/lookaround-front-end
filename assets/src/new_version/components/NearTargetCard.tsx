import React from "react"
import { View , Text, TouchableOpacity} from "react-native"
import TargetData from "../data/TargetData";
import {
    useFonts,
    SourceSansPro_200ExtraLight,
    SourceSansPro_200ExtraLight_Italic,
    SourceSansPro_300Light,
    SourceSansPro_300Light_Italic,
    SourceSansPro_400Regular,
    SourceSansPro_400Regular_Italic,
    SourceSansPro_600SemiBold,
    SourceSansPro_600SemiBold_Italic,
    SourceSansPro_700Bold,
    SourceSansPro_700Bold_Italic,
    SourceSansPro_900Black,
    SourceSansPro_900Black_Italic,
  } from '@expo-google-fonts/source-sans-pro';
import { Colors } from "react-native/Libraries/NewAppScreen";
import * as Utils from "../Utils"
import useLocation from "../data/redux/useLocation";

import * as Iconattor from "../Iconattor"
import useFocussedTarget from "../data/redux/useFocussedTarget";
import { useNavigation } from "@react-navigation/native";

interface NearTargetCardProps{
    target: TargetData;
}
const NearTargetCard = (props: NearTargetCardProps) =>{

    const [location] = useLocation();
    const target = props.target;
    const h = 65;

    const [focussedTarget, focus] = useFocussedTarget();
    const navigation = useNavigation();

    const onPress = () =>{
        focus(props.target);
        navigation.navigate("Explore");
    }

    let [fontsLoaded] = useFonts({
        SourceSansPro_200ExtraLight,
        SourceSansPro_200ExtraLight_Italic,
        SourceSansPro_300Light,
        SourceSansPro_300Light_Italic,
        SourceSansPro_400Regular,
        SourceSansPro_400Regular_Italic,
        SourceSansPro_600SemiBold,
        SourceSansPro_600SemiBold_Italic,
        SourceSansPro_700Bold,
        SourceSansPro_700Bold_Italic,
        SourceSansPro_900Black,
        SourceSansPro_900Black_Italic,
      });

    if(!fontsLoaded){
        return <View style={{height:h, marginBottom:30}}></View>
    }
    return(
        <TouchableOpacity style={{height:h, flexDirection:"row", marginBottom:10}} onPress={onPress}>
            <View style={{width:h, marginRight:10}}>
                <View style={{flex:1, margin:6}}>
                    <View style={{flex:1, borderRadius:100, backgroundColor:Colors.white, opacity:0.7}}>
                    </View>
                    <View style={{flex:1, position:"absolute", left:0, top:0, bottom:0, right:0}}>
                        {Iconattor.getIconByTarget(target, 45)}
                    </View>
                </View>

                
                
            </View>
            <View style={{flex:1, justifyContent:"center"}}>
                <Text style={{fontSize:15, fontFamily:"SourceSansPro_900Black", color: Colors.white}}>
                    {Utils.formatDistance(Utils.calculateDistance(target.location,location))}
                </Text>
                <Text numberOfLines={2} style={{fontSize:20, fontFamily:"SourceSansPro_700Bold", color: Colors.white}}>
                    {target.name}
                </Text>
            </View>
            
            
        </TouchableOpacity>
    )
}

export default NearTargetCard;