export const Spacing = {
    s1: 64,
    s2: 32,
    s3: 25,
    s4: 8,
    s5: 4
}

export const Colors = {
    primary: "#3987bd",//"#3e92cc",
    secondary: "#990d35",
    white: "#FFFFFF",
    light: "#f6f4f3",
    dark: "#333333",
    lightGrey: "#ddd"
}

export const FontSize = {
    h1: 64,
    h2: 32,
    h3: 16,
    h4: 8
} 
export const FontWeight : {w1: "900",w2: "700",w3: "500",w4: "300", w5: "100", normal: "normal"} = {
    w1: "900",
    w2: "700",
    w3: "500",
    w4: "300", 
    w5: "100",
    normal: "normal"
}

export const BorderWidth = {
    thin: 2,
    normal: 5,
    bold: 8
}

export const Width = {
    w1: 200,
    w2: 150,
    w3: 100,
    w4: 40,
    w5: 20
}